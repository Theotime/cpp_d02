/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 06:23:37 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 06:28:57 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __MAIN_HPP__
# define __MAIN_HPP__

# include "Fixed.class.hpp"
# include "Calculator.class.hpp"

# include <string>
# include <iostream>
# include <cmath>
# include <sstream>

#endif
