/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Calculator.class.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 06:26:22 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 06:29:15 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Calculator.class.hpp"
#include <regex>

std::pair	<std::string, Calculator::operations> Calculator::_operations[4] = {
	std::make_pair("+", &Calculator::addition),
	std::make_pair("-", &Calculator::substration),
	std::make_pair("*", &Calculator::multiplication),
	std::make_pair("/", &Calculator::division)
};

Calculator::Calculator(char *calcul) : _original(calcul) {}

Calculator::Calculator(Calculator const &obj) {
	*this = obj;
}

Calculator::~Calculator() {}


Calculator			&Calculator::operator=(Calculator const &obj) 
{
	this->_original = obj._original;
	return *this;
}

bool				Calculator::checkFormat(std::string calc)
{
	int		open = 0;
	int		close = 0;
	int		i = 0;

	if (!std::regex_match(calc, std::regex("^( *[(]* *[-]?[0-9.]+ *[)]*( +[+-/*]{1} + *[(]*[-]?[0-9.]+ *[)]*)* *)+$")))
		return (false);

	while (calc[i])
	{
		if (calc[i] == '(')
			++open;
		else if (calc[i] == ')')
			++close;
		++i;
	}
	return (open == close);
}

void				Calculator::resolve(void)
{
	if (!this->checkFormat(this->_original))
	{
		std::cout << "Bad format" << std::endl;
		return ;
	}
	std::cout << this->calculate(this->_original) << std::endl;
}

Fixed				Calculator::apply(std::string const op, Fixed const n1, Fixed const n2)
{
	(void)n1;(void)n2;
	for (int i = 0; i < 4; ++i)
	{
		if (Calculator::_operations[i].first == op)
			return (this->*Calculator::_operations[i].second)(n1, n2);
	}
	return Fixed(0);
}

// This function is very very bad, but it's 1am and i just want to sleep
Fixed				Calculator::calculateSimpleOperation(std::string str)
{
	Fixed				result;
	Fixed				*n1;
	Fixed				*n2;
	std::stringstream	calc;
	std::stringstream	op;
	int					i = 0;
	int					start = -1;
	int					end = -1;
	int					n = 0;

	while (str[i])
	{
		if (str[i] == '*' || str[i] == '/')
		{
			start = i - 1;
			end = i + 1;
			n = 0;
			while (start && str[start])
			{
				if (str[start] == ' '){
					if (n)
						break ;
				} else
					n = 1;
				--start;
			}
			end = i + 1;
			n = 0;
			while (str[end])
			{
				if (str[end] == ' ') {
					if (n)
						break ;
				} else
					n = 1;
				++end;
			}
			n1 = new Fixed(str.substr(start, i - start));
			n2 = new Fixed(str.substr(i + 1, end - i));
			calc.str("");
			op.str("");
			op << str[i];
			calc << str.substr(0, start) << this->apply(op.str(), *n1, *n2) << str.substr(end, str.length());
			delete n1;
			delete n2;
			str = calc.str();
			i = -1;
		}
		++i;
	}
	i = 0;
	while (str[i])
	{
		if (str[i] == '+' || (str[i] == '-' && str[i + 1] == ' '))
		{
			start = i - 1;
			end = i + 1;
			n = 0;
			while (start && str[start])
			{
				if (str[start] == ' '){
					if (n)
						break ;
				} else
					n = 1;
				--start;
			}
			end = i + 1;
			n = 0;
			while (str[end])
			{
				if (str[end] == ' ') {
					if (n)
						break ;
				} else
					n = 1;
				++end;
			}
			n1 = new Fixed(str.substr(start, i - start));
			n2 = new Fixed(str.substr(i + 1, end - i));
			calc.str("");
			op.str("");
			op << str[i];
			calc << str.substr(0, start) << this->apply(op.str(), *n1, *n2) << str.substr(end, str.length());
			delete n1;
			delete n2;
			str = calc.str();
			i = -1;
		}
		++i;
	}
	result = Fixed(str);

	return (result);
}

Fixed				Calculator::calculate(std::string str)
{
	Fixed				result;
	int					i = 0;
	int					n = 0;
	int					start = -1;
	int					end = -1;
	std::stringstream	calc;

	while (str[i]) {
		if (str[i] == '(') {
			++n;
			if (start == -1)
				start = i;
		} else if (str[i] == ')') {
			--n;
			if (n == 0) {
				end = i;
				break ;
			}
		}

		++i;
	}

	if (end != -1) {
		result = this->calculate(str.substr(start + 1, end - start - 1));
		calc << str.substr(0, start) << result << str.substr(end + 1, str.length());
		result = this->calculate(calc.str());
	} else {
		result = this->calculateSimpleOperation(str);
	}

	return (result);
}

Fixed				Calculator::addition(Fixed const n1, Fixed const n2)
{
	return (n1 + n2);
}

Fixed				Calculator::substration(Fixed const n1, Fixed const n2)
{
	return (n1 - n2);
}

Fixed				Calculator::multiplication(Fixed const n1, Fixed const n2)
{
	return (n1 * n2);
}

Fixed				Calculator::division(Fixed const n1, Fixed const n2)
{
	return (n1 / n2);
}