/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Calculator.class.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 06:26:24 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 06:28:08 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __CALCULATOR_HPP__
# define __CALCULATOR_HPP__

# include <string>
# include <iostream>
# include <cmath>
# include <sstream>

# include "Fixed.class.hpp"

class Calculator {
	public:
		typedef Fixed		(Calculator::*operations)(Fixed const n1, Fixed const n2);

		Calculator(char *calcul);
		Calculator(Calculator const &obj);
		~Calculator();

		void				resolve(void);


		Calculator			&operator=(Calculator const &obj);

	private:
		static std::pair <std::string, operations> _operations[4];
		std::string			_original;

		bool				checkFormat(std::string calc);

		Fixed				calculate(std::string str);
		Fixed				apply(std::string const op, Fixed const n1, Fixed const n2);
		Fixed				addition(Fixed const n1, Fixed const n2);
		Fixed				substration(Fixed const n1, Fixed const n2);
		Fixed				multiplication(Fixed const n1, Fixed const n2);
		Fixed				division(Fixed const n1, Fixed const n2);
		Fixed				calculateSimpleOperation(std::string str);
		
};

#endif
