/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 05:43:50 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 06:36:03 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.hpp"
#include "Calculator.class.hpp"

int			main(int ac, char **av) {
	Calculator		*cal;

	if (ac != 2)
		return (1);

	cal = new Calculator(av[1]);
	cal->resolve();

	return (0);
}
