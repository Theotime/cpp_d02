/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 20:09:12 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 22:06:24 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FIXED_CLASS_HPP__
# define __FIXED_CLASS_HPP__

# define FRACTIONAL_BITS 8

# include <string>
# include <iostream>

class Fixed {

	public:
		Fixed();
		Fixed(Fixed const &obj);
		~Fixed();

		int				getValue()					const;
		int				getRawBits()				const;

		void			setRawBits(int const raw);
		Fixed			&operator=(Fixed const &obj);

	private:
		int				_value;
		int const		_bits;

};


#endif
