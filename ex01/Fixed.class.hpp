/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 20:09:12 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 01:59:30 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FIXED_CLASS_HPP__
# define __FIXED_CLASS_HPP__

# define FRACTIONAL_BITS 8

# include <string>
# include <iostream>
# include <cmath>

class Fixed {

	public:
		Fixed();
		Fixed(Fixed const &obj);
		Fixed(int value);
		Fixed(float value);
		~Fixed();

		int				getRawBits()				const;

		void			setRawBits(int const raw);
		Fixed			&operator=(Fixed const &obj);

		float			toFloat()					const;
		int				toInt()						const;

	private:
		int				_value;
		int const		_bits;

};

std::ostream			&operator<<(std::ostream &o, Fixed const &src);

#endif
